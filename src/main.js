import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'

Vue.config.productionTip = false

Vue.component('vue-image', () => import('@/components/VueImage'))
Vue.component('vue-background-image', () => import('@/components/VueBackgroundImage'))

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
