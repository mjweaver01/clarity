const path = require('path')
const StyleLintPlugin = require('stylelint-webpack-plugin')
const PreloadWebpackPlugin = require('preload-webpack-plugin')
const ImageminWebpWebpackPlugin = require('imagemin-webp-webpack-plugin')

const baseURL = 'https://claritysallyday.com'
const availableRoutes = [
  '/',
  '/about',
  '/discover',
  '/connect'
]

module.exports = {
  runtimeCompiler: true,

  pwa: {
    name: 'Clarity',
    themeColor: '#95c83e',
    msTileColor: '#95c83e',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: '#95c83e',
    workboxPluginMode: 'GenerateSW',
    workboxOptions: {
      exclude: [/\.map$/, /_redirects/, /netlify.toml/]
    }
  },

  chainWebpack: config => {
    config.resolve.symlinks(false)

    config.module
      .rule('eslint')
      .use('eslint-loader')
      .options({
        fix: true
      })

    config
      .plugin('html')
      .tap((args) => {
        args[0].title = 'Clarity'
        return args
      })
  },

  css: {
    loaderOptions: {
      sass: {
        prependData: `
          @import "@/styles/_variables.scss";
        `,
        sassOptions: {
          scoped: false
        }
      }
    }
  },

  configureWebpack: {
    plugins: [
      new StyleLintPlugin({
        configFile: './.stylelintrc',
        files: ['src/**/*.{vue,html,css,scss}'],
        fix: true
      }),
      new PreloadWebpackPlugin({
        rel: 'preload',
        as: 'style',
        include: ['allChunks'],
        fileBlacklist: [/\.map|.js/]
      }),
      new ImageminWebpWebpackPlugin({
        config: [{
          test: /\.(jpe?g|png)/,
          options: {
            quality: 75
          }
        }],
        overrideExtension: false,
        detailedLogs: false,
        silent: false,
        strict: true
      })
    ]
  },

  pluginOptions: {
    prerenderSpa: {
      registry: undefined,
      renderRoutes: availableRoutes,
      useRenderEvent: true,
      headless: true,
      onlyProduction: true,
      postProcess: route => {
        // Remove /index.html from the output path if the dir name ends with a .html file extension.
        // For example: /dist/dir/special.html/index.html -> /dist/dir/special.html
        if (route.route.endsWith('.html')) {
          route.outputPath = path.join(__dirname, 'dist', route.route)
        }

        // Defer scripts and tell Vue it's been server rendered to trigger hydration
        route.html = route.html
          .replace(/<script (.*?)>/g, '<script $1 defer>')
          .replace('id="app"', 'id="app" data-server-rendered="true"')
        return route
      }
    },

    sitemap: {
      baseURL,
      urls: availableRoutes
    }
  }
}
