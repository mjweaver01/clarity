# Clarity

[![Netlify Status](https://api.netlify.com/api/v1/badges/70222f39-ac90-402d-87c5-a8b0c8921766/deploy-status)](https://app.netlify.com/sites/marquee-clarity/deploys)

This provides the following support along with the normal Vue CLI features:

- Linting and autoformatting for JS/SCSS
- Prerender
- Sitemap

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
